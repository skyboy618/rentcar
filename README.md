**Description**
Web service for car rental company

**Tools**
Java, Spring, Hibernate, MySQL, Angular, Node Js and Npm, Typescript, Bootstrap

**Features**
-  various ways of search
 

**Requirements**
- Java
- MySql
- Maven



**Run, test**


Future updates will include: 
-
- Possibility to search for a car after it's category.
- sort cars by year / price per day
- login form with user type
- customer will receive an email when making a rental request in which he will be informed if his
    request is accepted by the admin or not
- better project structure + refactor
- improve design