package com.sda.rentcar.mapper;

import com.sda.rentcar.dto.CarDto;
import com.sda.rentcar.dto.WarehouseDto;
import com.sda.rentcar.model.Car;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CarMapper {


    public static List<CarDto> toCarDto(List<Car> carList) {
        List<CarDto> carSummaryDtos = new ArrayList<>();

        carList.stream()
                .map(entity -> carSummaryDtos.add(entityToCarDto(entity)))
                .collect(Collectors.toList());

        return carSummaryDtos;
    }

    public static CarDto entityToCarDto(Car car) {
        CarDto carDto = new CarDto();
        carDto.setId(car.getId());
        carDto.setBrand(car.getBrand());
        carDto.setColor(car.getColor());
        carDto.setModel(car.getModel());
        carDto.setIsAvailable(car.getIsAvailable());
        carDto.setFabricationYear(car.getFabricationYear());
        carDto.setPricePerDay(car.getPricePerDay());
//        WarehouseDto warehouseDto = WarehouseMapper.entitytoDto(car.getWarehouse());
//        carDto.set(warehouseDto);
//        carDto.setWarehouse(car.getWarehouse().getAddress());
        return carDto;
    }
}
