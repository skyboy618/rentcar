package com.sda.rentcar.mapper;

import com.sda.rentcar.dto.CarDto;
import com.sda.rentcar.dto.WarehouseDto;
import com.sda.rentcar.model.Warehouse;

import java.util.List;

import static com.sda.rentcar.mapper.CarMapper.toCarDto;


public class WarehouseMapper {

    public static WarehouseDto entitytoDto(Warehouse warehouse){
        WarehouseDto warehouseDto = new WarehouseDto();
        warehouseDto.setId(warehouse.getId());
        warehouseDto.setAddress(warehouse.getAddress());
      List<CarDto> carList = CarMapper.toCarDto(warehouse.getCarList());
        warehouseDto.setCarList(carList);

        return warehouseDto;
    }
}
