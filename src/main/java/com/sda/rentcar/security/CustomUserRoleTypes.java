package com.sda.rentcar.security;

public enum CustomUserRoleTypes {

    ADMIN,
    USER,
    GUEST

}