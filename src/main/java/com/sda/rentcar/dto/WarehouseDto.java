package com.sda.rentcar.dto;

import java.util.List;

public class WarehouseDto {

    private Long id;

    private String address;

    private List<CarDto> carList;

//    private List<CarSummaryDto> carList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

//    public List<CarSummaryDto> getCarList() {
//        return carList;
//    }
//
//    public void setCarList(List<CarSummaryDto> carList) {
//        this.carList = carList;
//    }

    public List<CarDto> getCarList() {
        return carList;
    }

    public void setCarList(List<CarDto> carList) {
        this.carList = carList;
    }
}
