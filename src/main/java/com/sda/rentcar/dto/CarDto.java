package com.sda.rentcar.dto;

public class CarDto {

    private Long id;

    private String brand;

    private String model;

    private String color;

    private int fabricationYear;

    private Boolean isAvailable;

    private Double pricePerDay;

//    private Warehouse warehouse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getFabricationYear() {
        return fabricationYear;
    }

    public void setFabricationYear(int fabricationYear) {
        this.fabricationYear = fabricationYear;
    }


    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean available) {
        isAvailable = available;
    }

//    public WarehouseDto getWarehouse() {
//        return warehouse;
//    }
//
//    public void setWarehouse(WarehouseDto warehouse) {
//        this.warehouse = warehouse;
//    }


//    public String getWarehouse() {
//        return warehouse;
//    }
//
//    public void setWarehouse(String warehouse) {
//        this.warehouse = warehouse;
//    }
}
