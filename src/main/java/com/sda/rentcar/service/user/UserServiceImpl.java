package com.sda.rentcar.service.user;

import com.sda.rentcar.dto.UserDto;
import com.sda.rentcar.model.Role;
import com.sda.rentcar.model.User;
import com.sda.rentcar.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private static final long EXPIRE_TOKEN_AFTER_MINUTES = 30;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public void save(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setEmail(user.getEmail());
        user.setFullName(user.getFullName());
        user.setPassword(user.getPassword());
        user.setPhone(userDto.getPhone());
        user.setToken(userDto.getToken());
        user.setTokenCreationDate(userDto.getTokenCreationDate());

        List<User> users = userRepository.findAll();

        if (users.size() == 0) {
            user.setRole(Role.valueOf("Administrator"));
        } else {
            user.setRole(Role.valueOf("Standard"));
        }
        if (!ifEmailExist(user.getEmail())) {
            userRepository.save(user);
        }
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public List<UserDto> findAll() {
        List<User> users = userRepository.findAll();
        List<UserDto> usersDto = new ArrayList<>();

        for (User user : users) {
            UserDto userDto = new UserDto();
            userDto.setId(user.getId());
            userDto.setAddress(user.getAddress());
            userDto.setEmail(user.getEmail());
            userDto.setFullName(user.getFullName());
            userDto.setPassword(user.getPassword());
            userDto.setPhone(user.getPhone());
            userDto.setToken(user.getToken());
            userDto.setTokenCreationDate(user.getTokenCreationDate());
            userDto.setRole(user.getRole().name());

            usersDto.add(userDto);
        }
        return usersDto;
    }

    @Override
    public UserDto findById(Long id) {

        Optional<User> userModel = userRepository.findById(id);
        UserDto userDto = new UserDto();
        if (userModel.isPresent()) {
            userDto.setId(userModel.get().getId());
            userDto.setEmail(userModel.get().getEmail());
            userDto.setPassword(userModel.get().getPassword());
            userDto.setRole(userModel.get().getRole().name());
            userDto.setToken(userModel.get().getToken());
            userDto.setTokenCreationDate(userModel.get().getTokenCreationDate());
            userDto.setPhone(userModel.get().getPhone());
            userDto.setFullName(userModel.get().getFullName());

        }
        return userDto;

    }

    public UserDto findByUsername(String username) {
        Optional<User> userModelOptional = userRepository.getUserByEmail(username);
        UserDto userDto = new UserDto();
        if (userModelOptional.isPresent()) {
            User userModel = userModelOptional.get();
            userDto.setId(userModel.getId());
            userDto.setEmail(userModel.getEmail());
            userDto.setPhone(userModel.getPhone());
            userDto.setFullName(userModel.getFullName());
            userDto.setPassword(userModel.getPassword());
            userDto.setRole(userModel.getRole().name());
        }
        return userDto;
    }


    @Override
    public void update(UserDto userDto) {
        User user = convertUser(userDto);
        if (!ifEmailExist(user.getEmail())) {
            userRepository.save(user);
        }
    }


    public User convertUser(UserDto userDto) {
        Optional<User> newUser = userRepository.findById(userDto.getId());
        User user = new User();
        if (newUser.isPresent()) {
            user = newUser.get();
            user.setId(userDto.getId());
            user.setRole(Role.valueOf(userDto.getRole()));
            user.setEmail(userDto.getEmail());
            user.setPhone(userDto.getPhone());
            user.setPassword(userDto.getPassword());
            user.setFullName(userDto.getFullName());
            user.setToken(userDto.getToken());
            user.setTokenCreationDate(userDto.getTokenCreationDate());
            user.setAddress(userDto.getAddress());
        }
        return user;
    }


    private boolean ifEmailExist(String email) {
        List<User> users = userRepository.findAll();
        for (User user : users) {
            if (email.equals(user.getEmail())) {
                return true;
            }
        }
        return false;
    }

    public String forgotPassword(String email) {
        Optional<User> userOptional = userRepository.findUserByEmail(email);
        if (!userOptional.isPresent()) {
            return "invalid email";
        }

        User user = userOptional.get();
        user.setToken(generateToken());
        user.setTokenCreationDate(LocalDateTime.now());
        user = userRepository.save(user);
        return user.getToken();
    }


    public String resetPassword(String token, String password) {
        Optional<User> userOptional = userRepository.findByToken(token);
        if (!userOptional.isPresent()) {
            return "invalid token.";
        }
        LocalDateTime tokenCreationDate = userOptional.get().getTokenCreationDate();

        if (isTokenExpired(tokenCreationDate)){
            return "Token expired";
        }

        User user = userOptional.get();
        user.setPassword(password);
        user.setTokenCreationDate(null);
        user.setToken(null);
        userRepository.save(user);

        return "Your password has been successfully updated";
    }


    private String generateToken() {
        StringBuilder token = new StringBuilder();

        return token.append(UUID.randomUUID().toString())
                .append(UUID.randomUUID().toString()).toString();
    }


    private boolean isTokenExpired(final LocalDateTime tokenCreationDate) {

        LocalDateTime now = LocalDateTime.now();
        Duration diff = Duration.between(tokenCreationDate, now);

        return diff.toMinutes() >= EXPIRE_TOKEN_AFTER_MINUTES;
    }

}


