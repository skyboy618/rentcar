package com.sda.rentcar.service.user;

import com.sda.rentcar.dto.UserDto;
import com.sda.rentcar.model.User;

import java.util.List;

public interface UserService {


    public void save(UserDto userDto);

    public void deleteById(Long id);

    public List<UserDto> findAll();

    public UserDto findByUsername(String username);

    public UserDto findById(Long id);

    public void update(UserDto userDto);

    public String forgotPassword(String email);

    public String resetPassword(String token, String password);
}
