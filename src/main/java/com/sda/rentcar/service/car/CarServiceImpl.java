package com.sda.rentcar.service.car;

import com.sda.rentcar.dto.CarDto;
import com.sda.rentcar.exception.CarNotFoundException;
import com.sda.rentcar.mapper.CarMapper;
import com.sda.rentcar.model.Car;
import com.sda.rentcar.repository.CarRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarServiceImpl implements CarService {

    private CarRepository carRepository;
    private ModelMapper modelMapper;

    @Autowired
    CarServiceImpl(CarRepository carRepository, ModelMapper modelMapper) {
        this.carRepository = carRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public CarDto create(Car car) {
        Car resultAfterSave = carRepository.save(car);
        CarDto response = modelMapper.map(resultAfterSave, CarDto.class);
        return response;
    }

    @Override
    public List<Car> saveAllCars(List<Car> cars) {
        return (List<Car>) carRepository.saveAll(cars);
    }

    @Override
    public CarDto getById(Long id) {
        Optional<Car> foundCar = carRepository.findById(id);

//        modelMapper.typeMap(Car.class, CarDto.class)
//                .addMappings(mapper -> mapper.map(Car::getWarehouse, CarDto::setWarehouse));

        return foundCar.map(entity -> modelMapper.map(entity, CarDto.class))
                .orElseThrow(() -> new CarNotFoundException("Car not found"));

    }

    @Override
    public CarDto update(Long id, Car car) {

        Optional<Car> foundCar = carRepository.findById(id);
        if (!foundCar.isPresent()) {
            throw new CarNotFoundException("couldn't update because car doesn't exist");
        } else {
            car.setId(id);
            Car entity =  carRepository.save(car);
           return CarMapper.entityToCarDto(entity);
        }
    }

    @Override
    public List<CarDto> findByBrand(String brand) {
        Optional<List<Car>> entities = carRepository.findByBrand(brand);
        return CarMapper.toCarDto(entities.orElseThrow(() -> new CarNotFoundException("Car not found")));

    }

    @Override
    public List<CarDto> findByModel(String model) {
        Optional<List<Car>> entities = carRepository.findByModel(model);
        return CarMapper.toCarDto(entities.orElseThrow(() -> new CarNotFoundException("Car not found")));
    }

    @Override
    public List<CarDto> getAll() {
        List<Car> entities = (List<Car>) carRepository.findAll();
        return CarMapper.toCarDto(entities);
    }

    @Override
    public void delete(Long id) {
        carRepository.deleteById(id);
        System.out.println("Car with id " + id +" has been deleted");
    }

    @Override
    public String getLocationFromCar(Long id) {
       return carRepository.findById(id).get().getWarehouse().getAddress();
    }
}
