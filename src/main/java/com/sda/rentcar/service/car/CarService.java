package com.sda.rentcar.service.car;

import com.sda.rentcar.dto.CarDto;
import com.sda.rentcar.model.Car;

import java.util.List;

public interface CarService {

    CarDto create(Car car);

    List<Car> saveAllCars(List<Car> cars);

    CarDto getById(Long id);

    CarDto update(Long id, Car car);

    List<CarDto> findByBrand(String brand);

    List<CarDto> findByModel(String model);

    List<CarDto> getAll();

    void delete(Long id);

    String getLocationFromCar(Long id);
}
