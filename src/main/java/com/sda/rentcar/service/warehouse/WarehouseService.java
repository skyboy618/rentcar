package com.sda.rentcar.service.warehouse;

import com.sda.rentcar.dto.WarehouseDto;
import com.sda.rentcar.model.Warehouse;


import java.util.List;

public interface WarehouseService {
    
    WarehouseDto create(Warehouse warehouse);

    WarehouseDto getById(Long id);

    WarehouseDto update(Long id, Warehouse warehouse);

    List<WarehouseDto> getAll();

    void delete(Long id);
}
