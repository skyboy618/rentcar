package com.sda.rentcar.service.warehouse;

import com.sda.rentcar.dto.WarehouseDto;
import com.sda.rentcar.exception.CarNotFoundException;
import com.sda.rentcar.exception.WarehouseNotFoundException;
import com.sda.rentcar.mapper.CarMapper;
import com.sda.rentcar.mapper.WarehouseMapper;
import com.sda.rentcar.model.Car;
import com.sda.rentcar.model.Warehouse;
import com.sda.rentcar.repository.WarehouseRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    private WarehouseRepository warehouseRepository;
    private ModelMapper modelMapper;
    private WarehouseMapper warehouseMapper;

    public WarehouseServiceImpl(WarehouseRepository warehouseRepository, ModelMapper modelMapper) {
        this.warehouseRepository = warehouseRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public WarehouseDto create(Warehouse warehouse) {
        Warehouse resultAfterSave = warehouseRepository.save(warehouse);
        WarehouseDto response = modelMapper.map(resultAfterSave, WarehouseDto.class);
        return response;
    }

    @Override
    public WarehouseDto getById(Long id) {
        Optional<Warehouse> foundWarehouse = warehouseRepository.findById(id);
//        modelMapper.map(Warehouse.class, WarehouseDto.class);
//                .addMappings(mapper -> mapper.map(Warehouse::getCarList, WarehouseDto::setCarList));
        return foundWarehouse.map(entity -> modelMapper.map(entity, WarehouseDto.class))
                .orElseThrow(() -> new WarehouseNotFoundException("Warehouse not found"));
    }

    @Override
    public WarehouseDto update(Long id, Warehouse warehouse) {
        Optional<Warehouse> foundWarehouse = warehouseRepository.findById(id);
        if (!foundWarehouse.isPresent()) {
            throw new WarehouseNotFoundException("couldn't update because warehouse doesn't exist");
        } else {
            warehouse.setId(id);
            Warehouse entity =  warehouseRepository.save(warehouse);
            return WarehouseMapper.entitytoDto(entity);
        }
    }

    @Override
    public List<WarehouseDto> getAll() {
        List<Warehouse> warehouseList = (List<Warehouse>) warehouseRepository.findAll();

        modelMapper.map(Warehouse.class, WarehouseDto.class);
//                .addMappings(mapper -> mapper.map(Warehouse::getCarList, WarehouseDto::setCarList));
        return warehouseList.stream()
                .map(entity -> modelMapper.map(entity, WarehouseDto.class))
                .collect(Collectors.toList());


    }

    @Override
    public void delete(Long id) {
        warehouseRepository.deleteById(id);
    }
}
