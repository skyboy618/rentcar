package com.sda.rentcar.service.customer;

import com.sda.rentcar.model.Customer;

import java.util.List;

public interface CustomerService {


    Customer saveCustomer(Customer customer);

    List<Customer> saveAllCustomers(List<Customer> customers);

    Customer findById(Long id);

    Customer findByEmail(String email);

    Customer updateCustomer(Long id, Customer customer);

    List<Customer> getAllCustomers();

    void delete(Long id);



}
