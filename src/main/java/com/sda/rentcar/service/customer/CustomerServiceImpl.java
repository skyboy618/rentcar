package com.sda.rentcar.service.customer;

import com.sda.rentcar.exception.CustomerNotFoundException;
import com.sda.rentcar.model.Customer;
import com.sda.rentcar.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {


    CustomerRepository customerRepository;

    @Autowired
    CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public Customer saveCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public List<Customer> saveAllCustomers(List<Customer> customers) {
        return (List<Customer>) customerRepository.saveAll(customers);
    }

    @Override
    public Customer findById(Long id) {
        Optional<Customer> foundCustomer = customerRepository.findById(id);
        return foundCustomer.orElseThrow(() -> new CustomerNotFoundException("Customer not found!"));
    }

    @Override
    public Customer findByEmail(String email) {
        return customerRepository.findCustomerByEmail(email);
    }

    @Override
    public Customer updateCustomer(Long id, Customer customer) {
        Optional<Customer> foundCustomer = customerRepository.findById(id);

        if (!foundCustomer.isPresent()){
            throw  new CustomerNotFoundException("Couldn't update because customer doesn't exist");
        }else {
            customer.setId(id);
            return customerRepository.save(customer);
        }
    }

    @Override
    public List<Customer> getAllCustomers() {
        return (List<Customer>) customerRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        customerRepository.deleteById(id);
    }
}
