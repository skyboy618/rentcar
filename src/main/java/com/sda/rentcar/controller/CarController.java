package com.sda.rentcar.controller;

import com.sda.rentcar.dto.CarDto;
import com.sda.rentcar.model.Car;
//import com.sda.rentcar.model.User;
import com.sda.rentcar.service.car.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins ="http://localhost:4200")
@RestController
@RequestMapping("/api/car")
public class CarController {

    private CarService carService;

    @Autowired
    CarController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping("/add")
    public ResponseEntity<CarDto> create(@Valid @RequestBody Car car) {
        return ResponseEntity.ok(carService.create(car));
    }

    @DeleteMapping("/delete/{id}")
    public void deleteCar(@PathVariable Long id) {
        carService.delete(id);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<CarDto> getById(@PathVariable Long id) {
        return ResponseEntity.ok(carService.getById(id));
    }

    @GetMapping("/{brand}")
    public ResponseEntity<List<CarDto>> getByBrand(@PathVariable String brand) {
        return ResponseEntity.ok(carService.findByBrand(brand));
    }

    @GetMapping("/{model}")
    public ResponseEntity<List<CarDto>> getByModel(@PathVariable String model) {
        return ResponseEntity.ok(carService.findByModel(model));
    }

    @GetMapping
    public List<CarDto> getAll() {
        return carService.getAll();
    }

    @PutMapping("/update/{id}")
    public CarDto update(@PathVariable Long id, @RequestBody Car car){
       return carService.update(id, car);
    }

    @GetMapping("/getAddress/{id}")
    public String getLocationFromCar( @PathVariable Long id){
        return carService.getLocationFromCar(id);
    }



}
