package com.sda.rentcar.controller;

import com.sda.rentcar.dto.CarDto;
import com.sda.rentcar.dto.WarehouseDto;
import com.sda.rentcar.model.Car;
import com.sda.rentcar.model.Warehouse;
import com.sda.rentcar.service.warehouse.WarehouseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/warehouse")
@CrossOrigin(origins ="http://localhost:4200")
public class WarehouseController {

    private WarehouseService warehouseService;


    public WarehouseController(WarehouseService warehouseService){
        this.warehouseService = warehouseService;

    }


    @PostMapping("/save")
    public ResponseEntity<WarehouseDto> create(@Valid @RequestBody Warehouse warehouse){
        return ResponseEntity.ok(warehouseService.create(warehouse));
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<WarehouseDto> getById(@PathVariable Long id){
        return ResponseEntity.ok(warehouseService.getById(id));
    }

    @GetMapping
    public ResponseEntity<List<WarehouseDto>> getAll(){
        return ResponseEntity.ok(warehouseService.getAll());
    }

    @DeleteMapping("/delete/{id}")
    public void deleteWarehouse(@PathVariable Long id){
        warehouseService.delete(id);
    }

    @PutMapping("/update/{id}")
    public WarehouseDto update(@PathVariable Long id, @RequestBody Warehouse warehouse){
        return warehouseService.update(id, warehouse);
    }




}
