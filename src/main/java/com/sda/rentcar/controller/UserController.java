package com.sda.rentcar.controller;


import com.sda.rentcar.dto.UserDto;
import com.sda.rentcar.repository.UserRepository;
import com.sda.rentcar.service.user.UserService;
import com.sda.rentcar.util.AuthenticationBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("api/user")
public class UserController {



    private UserService userService;

    public UserController(UserService userService){
        this.userService = userService;
    }



    @PostMapping("/save")
    public void addUser(@RequestBody UserDto userDto) {
        userService.save(userDto);

    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable(name = "id") Long id) {
        userService.deleteById(id);
    }

    @GetMapping
    public List<UserDto> getUser() {
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable(name = "id") Long id) {
        return userService.findById(id);
    }

    @PutMapping("/update")
    public void update(@RequestBody UserDto userDto) {
        userService.update(userDto);
    }






    @GetMapping(path = "/basicAuth")
    public AuthenticationBean basicAuth() {
        return new AuthenticationBean("You are authenticated");
    }

    @GetMapping("/{username}")
    public UserDto getUserByUsername(@PathVariable(name = "username") String userName){
        return userService.findByUsername(userName);
    }



    @PostMapping("/forgot-password/{email}")
    public String forgotPassword(@PathVariable(name = "email") String email) {

        String response = userService.forgotPassword(email);

        return response;
    }

    @PutMapping("/reset-password/{password}/{token}")   
    public void resetPassword(@PathVariable(name = "token") String token,
                                @PathVariable(name = "password") String password) {

        userService.resetPassword(token, password);
    }

}
