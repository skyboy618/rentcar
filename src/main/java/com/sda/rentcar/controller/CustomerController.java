package com.sda.rentcar.controller;

import com.sda.rentcar.model.Customer;
import com.sda.rentcar.service.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    CustomerController(CustomerService customerService){
        this.customerService = customerService;
    }

//    @PostMapping("/save")
//    public ResponseEntity<Customer> create(@RequestBody Customer customer){
//        return ResponseEntity.ok(customerService.saveCustomer(customer));
//    }

    @PostMapping("/register")
    public ResponseEntity<Customer> registerCustomer(@RequestBody Customer customer) {
        customerService.saveCustomer(customer);
        return new ResponseEntity("New account added", HttpStatus.CREATED);
    }

    @DeleteMapping("/delete")
    public void deleteCustomer(@RequestParam Long id){
        customerService.delete(id);
    }

    @PutMapping("/{id}")
    public Customer updateCustomer(@PathVariable Long id, @RequestBody Customer customer){
        return customerService.updateCustomer(id, customer);
    }

    @GetMapping
    public List<Customer> getAll(){
        return customerService.getAllCustomers();
    }




}
