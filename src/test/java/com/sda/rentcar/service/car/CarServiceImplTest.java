package com.sda.rentcar.service.car;

import com.sda.rentcar.dto.CarDto;
import com.sda.rentcar.exception.CarNotFoundException;
import com.sda.rentcar.model.Car;
import com.sda.rentcar.repository.CarRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class CarServiceImplTest {

    @Mock
    CarRepository mockCarRepository;

    @Mock
    ModelMapper mockModelMapper;

//    @Mock
//    ModelMapperComponent mockModelMapperComponent;

    @InjectMocks
    CarServiceImpl carServiceImpl;


    @ParameterizedTest
    @ValueSource(longs = {1L, 2L, 3L})
    void testGetCarById(Long id) {
        CarDto carDto = new CarDto();
        carDto.setId(id);
        Optional<Car> car = Optional.of(new Car());
        car.get().setId(id);
        Mockito.when(mockCarRepository.findById(id)).thenReturn(car);
//        Mockito.doNothing().when(mockModelMapperComponent).getCarCarInfoDtoTypeMap();
        Mockito.when(mockModelMapper.map(Mockito.any(), Mockito.any())).thenReturn(carDto);
        carServiceImpl.getById(id);
        Assertions.assertEquals(id, carDto.getId());
    }

    @Test
    void testGetCarByIdThrowsCarNotFoundException() {
        Mockito.when(mockCarRepository.findById(1L)).thenThrow(new CarNotFoundException("car not found"));

        Assertions.assertThrows(CarNotFoundException.class, () -> {
            carServiceImpl.getById(1L);
        });

    }

    @Test
    void testGetCarByIdThrowsException() {

        Mockito.when(mockCarRepository.findById(1L)).thenReturn(Optional.of(new Car()));
//        Mockito.doNothing().when(mockModelMapperComponent).getCarCarInfoDtoTypeMap();
        Mockito.when(mockModelMapper.map(Mockito.any(), Mockito.any())).thenReturn(null);

        Assertions.assertThrows(CarNotFoundException.class, () -> {
            carServiceImpl.getById(1L);
        });

    }

    @Test
    void testFindByModel() {
        List<Car> cars = new ArrayList<>();
        Car car = new Car();
        car.setModel("Dacia");
        cars.add(car);

        Mockito.when(mockCarRepository.findByModel("Dacia")).thenReturn(Optional.of(cars));
        List<CarDto> carDtoList = carServiceImpl.findByModel("Dacia");

        Assertions.assertEquals("Dacia", carDtoList.get(0).getModel());
        Assertions.assertEquals(1, carDtoList.size());
    }


}